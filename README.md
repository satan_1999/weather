# Weather Test #


### Install & Run ###

* This project is a simple Maven/Spring MVC project ( you may need to install maven plugin for eclipse , http://theopentutorials.com/tutorials/eclipse/installing-m2eclipse-maven-plugin-for-eclipse/).

* The main java source folder is Weather\src\main\java\au\com\wallace\weathertest\controller\.

* The main JS source folder is Weather\src\main\webapp\resources\js\

* Maven should download the libs, then you will be able to run this program in Tomcat

* The java VM is SDK 1.7( Should be Ok for 1.5 Above)


### Main Logic ###

* Visit http://localhost:8080/WeatherTest/
* Controller will load the cities from the property file
* Controller will return HTML template to browser
* Then the browser loaded the Javascript and rendered HTML
* Once users select the city in the selector, JS will load the weather data from Yahoo Weather Webservice.
* Then the JS will parse the JSON data and set the data into HTML elements

### Code Quality/Test ###

* The test source folder is Weather\src\test\java\au\com\wallace\weathertest\controller\
* The test case is against the controller and returned value