package au.com.wallace.weathertest.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.List;

import javax.swing.text.View;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring-test-servlet.xml" })
@WebAppConfiguration
public class WeatherControllerTest {

    @Value("#{'${options.cities}'.split(',')}")
    private List<String>  cities;
    private MockMvc       mockMvc;
    View                  mockView;
    @Autowired
    WebApplicationContext wac;


    @Before
    public void setUp() throws Exception
    {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void shouldReturnIndexViewAndCitiesAttribute() throws Exception
    {
        mockMvc.perform(get("/")).andExpect(status().isOk()).andExpect(model().attribute("cities", cities)).andExpect(view().name("index"));
    }

}
