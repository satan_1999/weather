<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"
  integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.min.css" rel="stylesheet" />
<title>Weather Forcast</title>
</head>
<body>
  <div class="container">
    <section id="basic" class="row">
    <div class="col-md-6">
      <h1>Weather Forcast :</h1>
    </div>
    </section>
  </div>

  <div class="container">
    <div class="row">
      <form role="form">
        <div class="col-md-4">
          <div class="well well-sm">
            <select id="citySelector" data-placeholder="Select a City">
            <option ></option>
              <c:forEach items="${cities}" var="city">
                <option value="${city}">${city}</option>
              </c:forEach>
            </select>
          </div>
          <div class="form-group">
            <label for="InputEmail">City</label>
            <div class="input-group col-md-12">
              <input type="text" class="form-control" readonly id="city" name="city">
            </div>
          </div>
          <div class="form-group">
            <label for="InputEmail">Updated Time</label>
            <div class="input-group col-md-12">
              <input type="text" class="form-control" readonly id="lastUpdated" name="lastUpdated">
            </div>
          </div>
          <div class="form-group">
            <label for="InputEmail">Weather</label>
            <div class="input-group col-md-12">
              <input type="text" class="form-control" readonly id="weather" name="weather">
            </div>
          </div>
          <div class="form-group">
            <label for="InputEmail">Temperature</label>
            <div class="input-group col-md-12">
              <input type="text" class="form-control" readonly id="temperature" name="temperature">
            </div>
          </div>
          <div class="form-group">
            <label for="InputEmail">Wind</label>
            <div class="input-group col-md-12">
              <input type="text" class="form-control" readonly id="wind" name="wind">
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>



  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"
    integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
  <script src="<c:url value="/resources/js/weather_test.js" />"></script>
</body>
</html>