/**
 * 
 */

jQuery(document).ready(function($) {
    $("#citySelector").select2({
        allowClear : true,
        width: '100%',
        dropdownAutoWidth : true
    });
    
    var getWeatherData = function(city){
        $.ajax({
            method: "GET",
            url: "https://query.yahooapis.com/v1/public/yql?u=c&q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22"+city+",Australia%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys",
            context: document.body
          }).done(function(data) {
              console.log("data", data);
              var channel = data.query.results.channel;
              $( "#city" ).val(channel.location.city +", "+channel.location.region+", "+channel.location.country);
              $( "#lastUpdated" ).val(channel.item.pubDate);
              $( "#weather" ).val(channel.item.condition.text);
              $( "#temperature" ).val(channel.item.condition.temp + " " + channel.units.temperature);
              $( "#wind" ).val(channel.wind.speed +" "+ channel.units.speed);
          });
    };
    $("#citySelector").on("change", function(e) {
        getWeatherData(this.value);
    });
});