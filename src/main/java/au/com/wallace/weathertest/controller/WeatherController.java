package au.com.wallace.weathertest.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class WeatherController {
    private static final Logger logger = Logger.getLogger(WeatherController.class);
    
    
    @Value("#{'${options.cities}'.split(',')}") 
    private List<String> cities;
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView index()
    {
        logger.debug("Calling index");
        ModelAndView model = new ModelAndView("index");
        model.addObject("cities", cities);
        return model;
    }

}
